<?php
class Buysell {
    /* Member variables */
    var $id, $tradeUid, $uid, $username, $amount, $currency, $betType, $startRate, $endRate, $timeline, $result, 
            $dateCreated, $dateUpdated;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
        
    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getTradeUid()
    {
        return $this->tradeUid;
    }

    /**
     * @param mixed $tradeUid
     */
    public function setTradeUid($tradeUid)
    {
        $this->tradeUid = $tradeUid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getBetType()
    {
        return $this->betType;
    }

    /**
     * @param mixed $betType
     */
    public function setBetType($betType)
    {
        $this->betType = $betType;
    }

    /**
     * @return mixed
     */
    public function getStartRate()
    {
        return $this->startRate;
    }

    /**
     * @param mixed $startRate
     */
    public function setStartRate($startRate)
    {
        $this->startRate = $startRate;
    }

    /**
     * @return mixed
     */
    public function getEndRate()
    {
        return $this->endRate;
    }

    /**
     * @param mixed $endRate
     */
    public function setEndRate($endRate)
    {
        $this->endRate = $endRate;
    }

    /**
     * @return mixed
     */
    public function getTimeline()
    {
        return $this->timeline;
    }

    /**
     * @param mixed $timeline
     */
    public function setTimeline($timeline)
    {
        $this->timeline = $timeline;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $betType
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getBuysell($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    // $dbColumnNames = array("id","uid","username","date_created","date_updated");
    $dbColumnNames = array("id","trade_uid","uid","username","amount","currency","bet_type","start_rate","end_rate",
                                "timeline","result","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"buy_sell");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        // $stmt->bind_result($id, $uid, $username, $dateCreated, $dateUpdated);
        $stmt->bind_result($id, $tradeUid, $uid, $username, $amount, $currency, $betType, $startRate, $endRate, $timeline, $result, 
                                $dateCreated, $dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $user = new Buysell;
            $user->setId($id);
            $user->setUid($uid);
            $user->setTradeUid($tradeUid);
            $user->setUsername($username);

            $user->setAmount($amount);
            $user->setCurrency($currency);
            $user->setBetType($betType);
            $user->setTimeline($timeline);
            $user->setResult($result);

            $user->setDateCreated($dateCreated);
            $user->setDateUpdated($dateUpdated);

            array_push($resultRows,$user);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
