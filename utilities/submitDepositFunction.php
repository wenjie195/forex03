<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function submitDeposit($conn,$uid,$username,$bankName,$amount,$date,$time,$reference,$status)
{
     if(insertDynamicData($conn,"deposit",array("uid","username","bank_name","amount","submit_date","submit_time","reference","status"),
     array($uid,$username,$bankName,$amount,$date,$time,$reference,$status),"ssssssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

    $uid = rewrite($_POST["deposit_uid"]);
    $name = rewrite($_POST["deposit_name"]);
    $bankName = rewrite($_POST["deposit_bank_name"]);

    $amount = rewrite($_POST["deposit_amount"]);
    $date = rewrite($_POST["deposit_date"]);
    $time = rewrite($_POST["deposit_time"]);

    $reference = rewrite($_POST["deposit_reference"]);
    $status = "PENDING";

     //   FOR DEBUGGING
     // echo "<br>";
     // echo $uid."<br>";
     // echo $name."<br>";
     // echo $bankName."<br>";
     // echo $amount."<br>";
     // echo $date."<br>";
     // echo $time."<br>";
     // echo $reference."<br>";
     // echo $status."<br>";

     if(submitDeposit($conn,$uid,$name,$bankName,$amount,$date,$time,$reference,$status))
     {
          // $_SESSION['messageType'] = 1;
          // header('Location: ../admin1Product.php?type=4');
          echo "submit deposit success";  
     }
     else
     {
          echo "fail to submit deposit success";
     }

}
else
{
     header('Location: ../submitDeposit.php');
}
?>
