<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Deposit.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $tz = 'Asia/Kuala_Lumpur';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $time = $dt->format('Y-m-d H:i:s');

    $uid = $_SESSION['uid'];
    $adminDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
    $adminUsername = $adminDetails[0]->getUsername();
    $approvedTime = $time;

    $deposit_status = "ACCEPTED";
    $depositID = rewrite($_POST["deposit_id"]);
    $depositUid = rewrite($_POST["deposit_uid"]);
    $depositUsername = rewrite($_POST["deposit_username"]);
    $depositAmount = rewrite($_POST["deposit_amount"]);
    //get uuser's uid and other details
    $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($depositUid),"s");
    $userFullname = $userDetails[0]->getFullName();
    $userDeposit = $userDetails[0]->getDeposit();
    $userCredit = $userDetails[0]->getCredit();

    $newDepositAmount = $userDeposit + $depositAmount;
    $newCreditAmount = $userCredit + $depositAmount;

    //for debugging
    // echo "<br>";
    // echo $depositID."<br>";
    // echo $depositUid."<br>";
    // echo $depositUsername."<br>";
    // echo $depositAmount."<br>";
    // echo $userFullname."<br>";
    // echo $userDeposit."<br>";
    // echo $userCredit."<br>";

    // echo $adminUsername."<br>";
    // echo $approvedTime."<br>";

    if(isset($_POST['deposit_id']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($newDepositAmount)
        {
            array_push($tableName,"deposit");
            array_push($tableValue,$newDepositAmount);
            $stringType .=  "s";
        }   
        if($newCreditAmount)
        {
            array_push($tableName,"credit");
            array_push($tableValue,$newCreditAmount);
            $stringType .=  "s";
        }   

        array_push($tableValue,$depositUid);
        $stringType .=  "s";
        $orderUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        
        if($orderUpdated)
        {
            echo "success";
            // $_SESSION['messageType'] = 1;
            // header('Location: ../adminShipping.php?type=11');


            if(isset($_POST['deposit_id']))
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($deposit_status)
                {
                    array_push($tableName,"status");
                    array_push($tableValue,$deposit_status);
                    $stringType .=  "s";
                }    
                if($adminUsername)
                {
                    array_push($tableName,"verify_by");
                    array_push($tableValue,$adminUsername);
                    $stringType .=  "s";
                } 
                if($approvedTime)
                {
                    array_push($tableName,"verify_time");
                    array_push($tableValue,$approvedTime);
                    $stringType .=  "s";
                }            
                array_push($tableValue,$depositID);
                $stringType .=  "s";
                $orderUpdated = updateDynamicData($conn,"deposit"," WHERE id = ? ",$tableName,$tableValue,$stringType);
                
                if($orderUpdated)
                {
                    echo "success aa";
                    // $_SESSION['messageType'] = 1;
                    // header('Location: ../adminShipping.php?type=11');
                }
                else
                {
                    echo "fail aa";
                }
            }
            else
            {
                echo "dunno aa";
            }
        }
        else
        {
            echo "fail";
        }
    }
    else
    {
        echo "dunno";
    }
}
else 
{
    header('Location: ../depositRequest.php');
}

?>