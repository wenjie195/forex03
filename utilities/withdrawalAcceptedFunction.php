<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Deposit.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $tz = 'Asia/Kuala_Lumpur';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $time = $dt->format('Y-m-d H:i:s');

    $uid = $_SESSION['uid'];
    $adminDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
    $adminUsername = $adminDetails[0]->getUsername();
    $approvedTime = $time;

    $withdrawal_status = "ACCEPTED";
    $withdrawalID = rewrite($_POST["withdrawal_id"]);
    $withdrawalReference = rewrite($_POST["withdrawal_reference"]);
    
    //for debugging
    // echo "<br>";
    // echo $withdrawalID."<br>";

    // echo $adminUsername."<br>";
    // echo $approvedTime."<br>";

    if(isset($_POST['withdrawal_id']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($withdrawal_status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$withdrawal_status);
            $stringType .=  "s";
        }   
        if($adminUsername)
        {
            array_push($tableName,"approved_by");
            array_push($tableValue,$adminUsername);
            $stringType .=  "s";
        }   
        if($approvedTime)
        {
            array_push($tableName,"approved_datetime");
            array_push($tableValue,$approvedTime);
            $stringType .=  "s";
        }   
        if($withdrawalReference)
        {
            array_push($tableName,"reference");
            array_push($tableValue,$withdrawalReference);
            $stringType .=  "s";
        }  

        array_push($tableValue,$withdrawalID);
        $stringType .=  "s";
        $orderUpdated = updateDynamicData($conn,"withdrawal"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        
        if($orderUpdated)
        {
            header('Location: ../adminWithdrawal.php');
        }
        else
        {
            echo "<script>alert('Fail to approved the withdrawal ');window.location='../adminWithdrawal.php'</script>";
        }
    }
    else
    {
        echo "<script>alert('ERROR !');window.location='../adminWithdrawal.php'</script>";
    }
}
else 
{
    header('Location: ../index.php');
}

?>