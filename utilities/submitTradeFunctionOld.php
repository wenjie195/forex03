<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../timezone.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

//trade with end rate
function submitTrade($conn,$tradeUID,$userUID,$rate,$endRate,$currency,$amount,$type,$timeline,$result,$currentCredit,$userUsername)
{
     if(insertDynamicData($conn,"record",array("trade_uid","uid","start_rate","end_rate","currency","amount","bet_type","timeline","result","current_credit","username"),
     array($tradeUID,$userUID,$rate,$endRate,$currency,$amount,$type,$timeline,$result,$currentCredit,$userUsername),"sssssisisss") === null)

     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}
function submitBetStatus($conn,$tradeUID,$userUID,$rate,$endRate,$currency,$amount,$type,$timeline,$result,$currentCredit,$userUsername,$resultEdited)
{

     if(insertDynamicData($conn,"bet_status",array("trade_uid","uid","start_rate","end_rate","currency","amount","bet_type","timeline","result","current_credit","username","result_edited"),
     array($tradeUID,$userUID,$rate,$endRate,$currency,$amount,$type,$timeline,$result,$currentCredit,$userUsername,$resultEdited),"sssssisissss") === null)

     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $tz = 'Asia/Kuala_Lumpur';
     $timestamp = time();
     $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
     $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
     $time = $dt->format('Y-m-d H:i:s');
     $playTime = $dt->format('s');

     //get ticker rating
     $curl = curl_init();
     curl_setopt_array($curl, array(
          CURLOPT_URL => "https://financialmodelingprep.com/api/v3/forex",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
               "x-rapidapi-host: currency-exchange.p.rapidapi.com",
               "x-rapidapi-key: c5875e8297msh333e08626d58b3fp1015afjsn73bce78f515f"
          ),
     ));

     $response = curl_exec($curl);
     $err = curl_error($curl);
     curl_close($curl);

     if ($err)
     {
          echo "cURL Error #:" . $err;
     }
     else
     {
          $exchangeRates = json_decode($response, true);
     }


     $tradeUID = md5(uniqid());
     $type = rewrite($_POST["trade_type"]);

     $timeline1 = rewrite($_POST["trade_timeline"]);
     $timeline2 = "30";

     if ($timeline1 != "")
     {
          $timeline = $timeline1;
     }
     else
     {
          $timeline = $timeline2;
     }

     $timeDate = ($_POST["date"]);
     $userUID = rewrite($_POST["user_uid"]);

     // $rate = rewrite($_POST["trade_rate"]);
     // $rateUp = rewrite($_POST["trading_rateup"]);
     // $rateDown = rewrite($_POST["trading_ratedown"]);

     $amount = rewrite($_POST["trade_amount"]);

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUID),"s");
     $userUsername = $userDetails[0]->getUsername();

     $userCurrentCredit = rewrite($_POST["user_credit"]);
     // echo $userCurrentCredit;
     $currentCredit = $userCurrentCredit;
     $playTime = rewrite($_POST["timer"]);

     $amount1 = rewrite($_POST["trade_amount"]);
     $amount2 = rewrite($_POST["trade_other_amount"]);
     if ($amount1 != "")
     {
          $amount = $amount1;
     }
     else
     {
          $amount = $amount2;
     }

     $currency = rewrite($_POST["trade_currency"]);

     // if ($exchangeRates)
     // {
     //      for ($cnt=0; $cnt <count($exchangeRates['forexList']) ; $cnt++)
     //      {
     //           $allCountryCurr = $exchangeRates['forexList'][$cnt]['ticker'];
     //           if ($allCountryCurr == $currency)
     //           {
     //                $rateUp = number_format($exchangeRates['forexList'][$cnt]['bid'], 4);
     //                $rateDown = number_format($exchangeRates['forexList'][$cnt]['ask'], 4);
     //           }
     //      }  
     // }
     // $currencyRate = rewrite($_POST["trade_currency_rate"]);

     //direct deduct user credit after trade
     $userCreditAfterTrade = $userCurrentCredit - $amount;

     if ($playTime == 0 || $playTime == 10 || $playTime == 20 || $playTime == 30 || $playTime == 40 || $playTime == 50)
     {
          if ($type == 'BUY')
          {
               // $rate = 4.4;
               
               if ($exchangeRates)
               {
                    for ($cnt=0; $cnt <count($exchangeRates['forexList']) ; $cnt++)
                    {
                         $allCountryCurr = $exchangeRates['forexList'][$cnt]['ticker'];
                         if ($allCountryCurr == $currency)
                         {
                              $rate = number_format($exchangeRates['forexList'][$cnt]['bid'], 4);
                              // $rateDown = number_format($exchangeRates['forexList'][$cnt]['ask'], 4);
                         }
                    }  
               }

               // $endRate = $rate;

               $editedRate = "0";
               $endRate = $rate + $editedRate;

               $result = 'DRAW';
               $userCredit = $userCurrentCredit;
               $resultEdited = $result;
          }
          elseif ($type == 'SELL')
          {
               // $rate = 4.4;

               if ($exchangeRates)
               {
                    for ($cnt=0; $cnt <count($exchangeRates['forexList']) ; $cnt++)
                    {
                         $allCountryCurr = $exchangeRates['forexList'][$cnt]['ticker'];
                         if ($allCountryCurr == $currency)
                         {
                              //$rate = number_format($exchangeRates['forexList'][$cnt]['bid'], 4);
                              $rate = number_format($exchangeRates['forexList'][$cnt]['ask'], 4);
                         }
                    }  
               }

               // $endRate = $rate;

               $editedRate = "0";
               $endRate = $rate + $editedRate;

               $result = 'DRAW';
               $userCredit = $userCurrentCredit;
               $resultEdited = $result;
          }
     }
     elseif($playTime <= 30)
     {
          if ($type == 'BUY')
          {
               // $rate = 4.4;

               if ($exchangeRates)
               {
                    for ($cnt=0; $cnt <count($exchangeRates['forexList']) ; $cnt++)
                    {
                         $allCountryCurr = $exchangeRates['forexList'][$cnt]['ticker'];
                         if ($allCountryCurr == $currency)
                         {
                              $rate = number_format($exchangeRates['forexList'][$cnt]['bid'], 4);
                              //$rateDown = number_format($exchangeRates['forexList'][$cnt]['ask'], 4);
                         }
                    }  
               }

               $editedRate = "0.0005";
               $endRate = $rate + $editedRate;
               $result = 'WIN';
               $userCredit = $userCurrentCredit + $amount;
               $resultEdited = $result;
          }
          elseif ($type == 'SELL')
          {
               // $rate = 4.4;

               if ($exchangeRates)
               {
                    for ($cnt=0; $cnt <count($exchangeRates['forexList']) ; $cnt++)
                    {
                         $allCountryCurr = $exchangeRates['forexList'][$cnt]['ticker'];
                         if ($allCountryCurr == $currency)
                         {
                              //$rateUp = number_format($exchangeRates['forexList'][$cnt]['bid'], 4);
                              $rate = number_format($exchangeRates['forexList'][$cnt]['ask'], 4);
                         }
                    }  
               }

               $editedRate = "0.0005";
               $endRate = $rate + $editedRate;
               $result = 'LOSE';
               $userCredit = $userCurrentCredit - $amount;
               $resultEdited = $result;
          }
     }
     else
     {
          if ($type == 'BUY')
          {
               // $rate = 4.4;

               if ($exchangeRates)
               {
                    for ($cnt=0; $cnt <count($exchangeRates['forexList']) ; $cnt++)
                    {
                         $allCountryCurr = $exchangeRates['forexList'][$cnt]['ticker'];
                         if ($allCountryCurr == $currency)
                         {
                              $rate = number_format($exchangeRates['forexList'][$cnt]['bid'], 4);
                              //$rateDown = number_format($exchangeRates['forexList'][$cnt]['ask'], 4);
                         }
                    }  
               }

               $editedRate = "0.0005";
               $endRate = $rate - $editedRate;
               $result = 'LOSE';
               $userCredit = $userCurrentCredit - $amount;
               $resultEdited = $result;
          }
          elseif ($type == 'SELL')
          {
               // $rate = 4.4;

               if ($exchangeRates)
               {
                    for ($cnt=0; $cnt <count($exchangeRates['forexList']) ; $cnt++)
                    {
                         $allCountryCurr = $exchangeRates['forexList'][$cnt]['ticker'];
                         if ($allCountryCurr == $currency)
                         {
                              //$rateUp = number_format($exchangeRates['forexList'][$cnt]['bid'], 4);
                              $rate = number_format($exchangeRates['forexList'][$cnt]['ask'], 4);
                         }
                    }  
               }

               $editedRate = "0.0005";
               $endRate = $rate - $editedRate;
               $result = 'WIN';
               $userCredit = $userCurrentCredit + $amount;
               $resultEdited = $result;
          }
     }

     $asdWin = "WIN";

     // //   FOR DEBUGGING
     // echo "aaaaa<br>";
     // echo $tradeUID."<br>";
     // echo $rate."<br>";
     // echo $type."<br>";
     // echo $timeline."<br>";
     // echo $timeDate."<br>";
     // echo $userUID."<br>";
     // echo $userCurrentCredit."<br>";
     // echo $playTime."<br>";
     // echo $amount."<br>";
     // echo $currency."<br>";
     // echo $userCreditAfterTrade."<br>";
     // echo $editedRate."<br>";
     // echo $endRate."<br>";
     // echo $result."<br>";
     // echo $userCredit."<br>";

     if ($amount == 0)
     {
          header('Location: ../userDashboard.php?type8787');
     }
     else
     {

          if ($userCurrentCredit < $amount)
          {
               header('Location: ../userDashboard.php?type44');
          }
          else
          {
               // echo "welcome";
               if(submitTrade($conn,$tradeUID,$userUID,$rate,$endRate,$currency,$amount,$type,$timeline,$result,$currentCredit,$userUsername))
               {
               // echo "1st data insert<br>";
                    if(submitBetStatus($conn,$tradeUID,$userUID,$rate,$endRate,$currency,$amount,$type,$timeline,$result,$currentCredit,$userUsername,$resultEdited))
                    {
                         // $_SESSION['messageType'] = 1;
                         // header('Location: ../userDashboard.php?type=4');
                         // echo "<script>alert('Done !');</script>";
                         // echo "2nd data insert<br>";

                         if(isset($_POST['submit_trade']))
                         // if($_SERVER['REQUEST_METHOD'] == 'POST')
                         {
                              $tableName = array();
                              $tableValue =  array();
                              $stringType =  "";
                              //echo "save to database";
                              if($userCredit)
                              {
                                   array_push($tableName,"credit");
                                   array_push($tableValue,$userCredit);
                                   $stringType .=  "s";
                              }
                              array_push($tableValue,$userUID);
                              $stringType .=  "s";
                              $deductCreditAfterTrade = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                              if($deductCreditAfterTrade)
                              {
                                   // echo "success deduct trade amount<br>";
                                   if($result == $asdWin)
                                   {
                                        // echo "u win<br>";

                                        $tableName = array();
                                        $tableValue =  array();
                                        $stringType =  "";
                                        //echo "save to database";
                                        if($userCredit)
                                        {
                                             array_push($tableName,"credit");
                                             array_push($tableValue,$userCredit);
                                             $stringType .=  "s";
                                        }
                                        array_push($tableValue,$userUID);
                                        $stringType .=  "s";
                                        $winGameUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                        if($winGameUpdated)
                                        {
                                             //sleep for 3 seconds
                                             // sleep(30);
                                             // echo "good game<br>";
                                             header('Location: ../userDashboard.php?type=1');
                                        }
                                        else
                                        {
                                             // echo "bad game<br>";
                                             // header('Location: ../userDashboard.php');
                                             header('Location: ../userDashboard.php?type=2');
                                        }
                                   }
                                   else
                                   {
                                        // echo "u lose<br>";
                                        header('Location: ../userDashboard.php?type=3');
                                   }
                              }
                              else
                              {
                                   // echo "fail to deduct trade amount";
                                   header('Location: ../userDashboard.php?type=4');
                              }
                         }
                         else
                         {
                         //     echo "dunno how to refund";
                         header('Location: ../userDashboard.php?type=5');
                         }
                    }
                    else
                    {
                         // echo "unable to insert 2nd data";
                         header('Location: ../userDashboard.php?type=6');
                    }
               }
               else
               {
                    // echo "unable to insert 1st data";
                    header('Location: ../userDashboard.php?type=7');
               }
          }
     }
}
else
{
     header('Location: ../index.php');
}
?>