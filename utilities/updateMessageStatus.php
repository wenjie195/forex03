<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Message.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $message_uid = rewrite($_POST["message_uid"]);
    $userStatus = "READ";

    //for debugging
    // echo "<br>";
    // echo $message_uid."<br>";
    // echo $message_details."<br>";

    if(isset($_POST['message_uid']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($userStatus)
        {
            array_push($tableName,"user_status");
            array_push($tableValue,$userStatus);
            $stringType .=  "s";
        } 

        array_push($tableValue,$message_uid);
        $stringType .=  "s";
        $messageUpdated = updateDynamicData($conn,"message"," WHERE message_uid = ? ",$tableName,$tableValue,$stringType);
        
        if($messageUpdated)
        {
            header('Location: ../viewMessage.php');
        }
        else
        {
            // echo "<script>alert('fail reply message!');window.location='../viewMessage.php'</script>"; 
            header('Location: ../viewMessage.php');
        }
    }
    else
    {
        header('Location: ../viewMessage.php');
    }

}
else 
{
    // echo "gg";
    header('Location: ../index.php');
}

?>