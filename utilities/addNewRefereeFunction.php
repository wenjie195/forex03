<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';

// $uid = $_SESSION['uid'];

function registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt,$phone,$userType)
{
     if(insertDynamicData($conn,"user",array("uid","username","email","password","salt","phone_no","user_type"),
          array($uid,$username,$email,$finalPassword,$salt,$phone,$userType),"ssssssi") === null)
     {
          header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}


function registerUpline($conn,$referralId,$referralName,$referrerId)
{
     if(insertDynamicData($conn,"referral_history",array("referral_id","referral_name","referrer_id"),
          array($referralId,$referralName,$referrerId),"sss") === null)
     {
          echo "unable to register";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

function sendEmailForVerification($uid) 
{
     $conn = connDB();
     $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

     $verifyUser_debugMode = 2;
     // $verifyUser_host = "mail.dxforextrade88.com";
     // $verifyUser_usernameThatSendEmail = "noreply@dxforextrade88.com";                   // Sender Acc Username
     // $verifyUser_password = "~sh~1z+kL=;C";                                                      // Sender Acc Password

     $verifyUser_host = "mail.ichibangame.com";
     $verifyUser_usernameThatSendEmail = "dxforextrade88@ichibangame.com";                   // Sender Acc Username
     $verifyUser_password = "x3iX4pApDoaZ";         

     $verifyUser_smtpSecure = "ssl";                                                           // SMTP type
     $verifyUser_port = 465;                                                                   // SMTP port no
     // $verifyUser_sentFromThisEmailName = "noreply@dxforextrade88.com";                                       // Sender Username
     // $verifyUser_sentFromThisEmail = "noreply@dxforextrade88.com";                       // Sender Email

     $verifyUser_sentFromThisEmailName = "dxforextrade88@ichibangame.com";                                       // Sender Username
     $verifyUser_sentFromThisEmail = "dxforextrade88@ichibangame.com";                       // Sender Email

     $verifyUser_sendToThisEmailName = $userRows[0]->getUsername();                                      // Recipient Username
     $verifyUser_sendToThisEmail = $userRows[0]->getEmail();                                   // Recipient Email
     $verifyUser_isHtml = true;                                                                // Set To Html
     $verifyUser_subject = "Welcome To dxforextrade88";                                      // Title

     $verifyUser_body = "<p>Dear ".$userRows[0]->getUsername().",</p>"; // Body
     $verifyUser_body .="<p>Welcome and thanks for choosing dxforextrade88 to make a trading.</p>";
     $verifyUser_body .="<p>Below is your account details : </p>";
     // $verifyUser_body .="<p>http://www.qlianmeng.asia</p>";
     $verifyUser_body .="<p>Username : ".$userRows[0]->getUsername()."</p>";
     // $verifyUser_body .="<p>Password : 123321</p>";
     $verifyUser_body .="<p>If you require any assistant, pls contact us.</p>";
     $verifyUser_body .="<p>Wish you have a happy trading with us!</p>";
     $verifyUser_body .="<p>Best Regards,</p>";
     $verifyUser_body .="<p>德鑫国际</p>";                                               // Body

    sendMailTo(
         null,
         $verifyUser_host,
         $verifyUser_usernameThatSendEmail,
         $verifyUser_password,
         $verifyUser_smtpSecure,
         $verifyUser_port, 
         $verifyUser_sentFromThisEmailName,
         $verifyUser_sentFromThisEmail,
         $verifyUser_sendToThisEmailName,
         $verifyUser_sendToThisEmail,
         $verifyUser_isHtml,
         $verifyUser_subject,
         $verifyUser_body,
         null
    );
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $username = rewrite($_POST['register_username']);
     // $register_fullname = rewrite($_POST['register_fullname']);

     $phone = rewrite($_POST['register_phone']);
     $email = rewrite($_POST['register_email_user']);

     $userType = rewrite($_POST['register_accounttype']);

     $register_password = rewrite($_POST['register_password']);
     $register_password_validation = strlen($register_password);
     $register_retype_password = rewrite($_POST['register_retype_password']);

     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     $referralName = rewrite($_POST['register_referral_name']);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $username."<br>";
     // echo $phone."<br>";
     // echo $email."<br>";
     // echo $userType."<br>";
     // echo $register_password."<br>";
     // echo $register_retype_password."<br>";
     // echo $password."<br>";
     // echo $salt."<br>";
     // echo $finalPassword."<br>";
     // echo $referralName."<br>";
     
          if($register_password == $register_retype_password)
          {
               if($register_password_validation >= 6)
               {
                    $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");
                    $usernameDetails = $usernameRows[0];

                    $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email_user']),"s");
                    $userEmailDetails = $userEmailRows[0];

                    $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($_POST['register_phone']),"s");
                    $userPhoneDetails = $userPhoneRows[0];

                    if (!$usernameDetails && !$userEmailDetails && !$userPhoneDetails)
                    {
                         if(registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt,$phone,$userType))
                         {
                              // sendEmailForVerification($register_uid);
                              // $_SESSION['messageType'] = 1;
                              // header('Location: ../profile.php?type=1');
                              // echo "register success";
                              // echo "<script>alert('Register Success !');window.location='../accountCreation.php'</script>";    

                              if($referralName != "")
                              {
     
                                   $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($referralName),"s");
                                   $uplineName = $usernameRows[0]->getUid();
                                   $referrerId = $uplineName;
     
                                   $referralId = $uid;
                                   $referralName = $username;
     
                                   if(registerUpline($conn,$referralId,$referralName,$referrerId))
                                   {     
                                        // $_SESSION['uid'] = $referralId;
                                        // $_SESSION['usertype_level'] = 1;
                                        sendEmailForVerification($uid);
                                        echo "<script>alert('Register Success With Upline !');window.location='../accountCreation.php'</script>";
                                   } 
                                   else
                                   { 
                                        echo "<script>alert('Unable to find referral name !');window.location='../accountCreation.php'</script>";
                                   }
                              }
                              else
                              { 
                                   sendEmailForVerification($uid);
                                   echo "<script>alert('Register Success !');window.location='../accountCreation.php'</script>";
                              }

                         }
                         else
                         { 
                              echo "<script>alert('Unable to register !');window.location='../accountCreation.php'</script>";
                         }
                    }
                    else
                    {
                         // header('Location: ../addReferee.php?promptError=1');
                         // echo "error registering new account.The account already exist";
                         echo "<script>alert('error registering new account.The account already exist');window.location='../accountCreation.php'</script>";
                    }
               }
               else 
               {
                    // $_SESSION['messageType'] = 1;
                    // header('Location: ../addReferee.php?type=5');
                    // echo "password must be more than 6";
                    echo "<script>alert('password must be more than 6');window.location='../accountCreation.php'</script>";
               }
          }
          else 
          {
               // $_SESSION['messageType'] = 1;
               // header('Location: ../addReferee.php?type=5');
               // echo "password and retype password not the same";
               echo "<script>alert('password and retype password not the same');window.location='../accountCreation.php'</script>";
          }      
}
else 
{
     header('Location: ../accountCreation.php');
}

?>