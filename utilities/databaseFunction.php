<?php
//***************************************SQL BUILDER START****************************************************/

function sqlSelectSimpleBuilder($columnNames,$tableName){
    $sql = "SELECT";
    $columnSize = count($columnNames);
    for ($i = 0; $i < $columnSize; $i++) {
        $sql .= " " . $columnNames[$i];
        if(($i + 1) >= $columnSize){
            $sql .=  " ";
        }else{
            $sql .=  ", ";
        }
    }

    $sql .= " FROM " . $tableName . " ";

//    if($queryColumns === NULL){
//
//    }else{
//        $whereSize = count($queryColumns);
//        for($x = 0; $x < $whereSize; $x++){
//            if($x === 0){
//                $sql .= " WHERE ";
//            }
//            $sql .= $queryColumns[$x] . " = ? ";
//            if(($x + 1) >= $whereSize){
//
//            }else{
//                $sql .= " AND ";
//            }
//        }
//    }

    return $sql;
}

function sqlInsertSimpleBuilder($columnNames,$tableName){
    $sql = "INSERT INTO $tableName (";
    $columnSize = count($columnNames);

    for ($i = 0; $i < $columnSize; $i++) {
        $sql .= " " . $columnNames[$i];
        if(($i + 1) >= $columnSize){
            $sql .=  " ";
        }else{
            $sql .=  ", ";
        }
    }

    $sql .= ") VALUES (";

    for ($x = 0; $x < $columnSize; $x++) {
        if(($x + 1) >= $columnSize){
            $sql .=  " ? ";
        }else{
            $sql .=  " ? , ";
        }
    }

    $sql .= ")";

    return $sql;
}

function sqlUpdateSimpleBuilder($columnNames,$tableName){
    $sql = "UPDATE $tableName SET";
    $columnSize = count($columnNames);

    for ($i = 0; $i < $columnSize; $i++) {
        $sql .= " " . $columnNames[$i];
        if(($i + 1) >= $columnSize){
            $sql .=  "  = ? ";
        }else{
            $sql .=  "  = ?, ";
        }
    }

    return $sql;
}

//***************************************SQL BUILDER END*******************************************************/


//***************************************SQL STATEMENT DYNAMIC BIND START****************************************************/

function returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes){
//    dynamically binding statement with values from here https://stackoverflow.com/questions/21896953/dynamically-bind-params-in-prepared-statements-with-mysqli
    //example
    //    $queryColumns = array("uid");
    //    $queryValues = array($uid);
    //    $queryTypes = 's';
    //    $user = getUser($conn,$queryColumns,$queryValues,$queryTypes);
    //    $user1 = getUser($conn);
    //    echo "should have result: " . $user->getEmail();
    //    echo "should have result: " . $user1->getEmail();
    $bind_names[] = $queryTypes;
    for ($i=0; $i<count($queryValues);$i++){
        $bind_name = 'bind' . $i;
        $$bind_name = $queryValues[$i];
        $bind_names[] = &$$bind_name;
    }
    $return = call_user_func_array(array($stmt,'bind_param'),$bind_names);

    return $stmt;
}

//***************************************SQL STATEMENT DYNAMIC BIND END******************************************************/

//***************************************COUNT START****************************************************/

function getCount($conn, $tableName = null, $countColumn = null, $whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    if(!$tableName || !$countColumn){
        return 0;
    }

    $sql = "SELECT COUNT($countColumn) AS total FROM $tableName ";
//    $sql = "SELECT COUNT(top_referrer_id) AS total FROM referral_history WHERE top_referrer_id = ?";

    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($totalCount);

        $stmt->fetch();

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return 0;
        }else{
            return $totalCount;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return 0;
    }
}

function getSum($conn, $tableName = null, $sumColumn = null, $whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    if(!$tableName || !$sumColumn){
        return 0;
    }

    $sql = "SELECT SUM($sumColumn) AS total FROM $tableName ";
//    $sql = "SELECT COUNT(top_referrer_id) AS total FROM referral_history WHERE top_referrer_id = ?";

    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($totalSum);

        $stmt->fetch();

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return 0;
        }else{
            return $totalSum;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return 0;
    }
}

//***************************************COUNT END****************************************************/


//***************************************INSERT START****************************************************/

function insertDynamicData($conn,$tableName = null,$columnNames = null,$columnValues = null,$columnTypes = null){
    if(!$tableName || !$columnNames || !$columnValues || !$columnTypes){
        return null;
    }

    $sql = sqlInsertSimpleBuilder($columnNames,$tableName);

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        $stmt = returnStmtWithDynamicBinding($stmt,$columnValues,$columnTypes);

        /* execute query */
        if(!$stmt->execute()){
//            echo $stmt->error;
            return null;
        }

        $newTableId = $stmt->insert_id;

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if(isset($newTableId) && $newTableId !== ''){
            return $newTableId;
        }else{
//            echo $conn->error;
            return null;
        }
    }else{
//        echo $conn->error;
        return null;
    }
}

//***************************************INSERT END****************************************************/


//***************************************UPDATE START****************************************************/

function updateDynamicData($conn,$tableName = null,$whereClause = null,$setColumnNamesOnly = null,$columnValuesAll = null,$columnTypesAll = null){
    if(!$setColumnNamesOnly || !$columnValuesAll || !$columnTypesAll){
        return null;
    }

    $sql = sqlUpdateSimpleBuilder($setColumnNamesOnly,$tableName);

    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        $stmt = returnStmtWithDynamicBinding($stmt,$columnValuesAll,$columnTypesAll);

        /* execute query */
        if(!$stmt->execute()){
//            echo $stmt->error;
            return null;
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        return true;
    }else{
//        echo $conn->error;
        return null;
    }
}

