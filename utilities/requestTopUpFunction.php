<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Message.php';
require_once dirname(__FILE__) . '/../classes/User.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function submitMessage($conn,$uid,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne)
{
     if(insertDynamicData($conn,"message",array("uid","message_uid","receive_message","user_status","admin_status","reply_one"),
     array($uid,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne),"ssssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();
     $uid = rewrite($_POST["usertopup_id"]);
     $message_uid = md5(uniqid());
     $receiveSMS = '(系统讯息) 要求充值，请联系我!';
     // $adminStatus = "GET";
     // $adminStatus = "TOPUP";
     $updateMessageStatus = "YES";

     $userStatus = "SENT";
     $adminStatus = "GET";
     $replyOne = $message_uid;

     //   FOR DEBUGGING
     // echo "<br>";
     // echo $uid."<br>";
     // echo $loginType."<br>";

          if(isset($_POST['usertopup_id']))
          {   
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database"; 
               if($updateMessageStatus)
               {
                    array_push($tableName,"message");
                    array_push($tableValue,$updateMessageStatus);
                    $stringType .=  "s";
               } 
          array_push($tableValue,$uid);
          $stringType .=  "s";
          $messageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($messageStatusInUser)
          {
               if(submitMessage($conn,$uid,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne))
               {
                    // $_SESSION['messageType'] = 1;
                    header('Location: ../viewMessage.php?type=1');
               }
               else
               {
                    header('Location: ../userDashboard.php?type=2');
               }
          }
          else
          {
               header('Location: ../viewMessage.php?type=3');
          }
     }
     else
     {
          header('Location: ../viewMessage.php?type=4');
     }


//     if(submitMessage($conn,$uid,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne))
//     {
//         // $_SESSION['messageType'] = 1;
//         header('Location: ../viewMessage.php?type=1');
//     }
//     else
//     {
//         header('Location: ../userDashboard.php?type=2');
//     }

}
else
{
     header('Location: ../index.php');
}
?>
