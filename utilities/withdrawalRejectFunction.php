<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Deposit.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $tz = 'Asia/Kuala_Lumpur';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $time = $dt->format('Y-m-d H:i:s');

    $uid = $_SESSION['uid'];
    
    $adminDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
    $adminUsername = $adminDetails[0]->getUsername();
    $rejectedTime = $time;

    $withdrawal_status = "REJECTED";
    $withdrawalID = rewrite($_POST["withdrawal_id"]);
    $withdrawalReference = rewrite($_POST["withdrawal_reference"]);
    $withdrawalUid = rewrite($_POST["withdrawal_uid"]);
    $withdrawalAmount = rewrite($_POST["withdrawal_amount"]);
    
    //recalculate credit
    $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($withdrawalUid),"s");
    $currentCredit = $userDetails[0]->getCredit();
    $refundCredit = $currentCredit + $withdrawalAmount;

    //for debugging
    // echo "<br>";
    // echo $withdrawalID."<br>";
    // echo $withdrawalUid."<br>";
    // echo $withdrawalReference."<br>";
    // echo $withdrawalAmount."<br>";
    
    // echo $currentCredit."<br>";
    // echo $refundCredit."<br>";

    // echo $adminUsername."<br>";
    // echo $rejectedTime."<br>";

    if(isset($_POST['withdrawal_id']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($withdrawal_status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$withdrawal_status);
            $stringType .=  "s";
        }   
        if($adminUsername)
        {
            array_push($tableName,"approved_by");
            array_push($tableValue,$adminUsername);
            $stringType .=  "s";
        }   
        if($rejectedTime)
        {
            array_push($tableName,"approved_datetime");
            array_push($tableValue,$rejectedTime);
            $stringType .=  "s";
        }   
        if($withdrawalReference)
        {
            array_push($tableName,"reference");
            array_push($tableValue,$withdrawalReference);
            $stringType .=  "s";
        }  

        array_push($tableValue,$withdrawalID);
        $stringType .=  "s";
        $orderUpdated = updateDynamicData($conn,"withdrawal"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        
        if($orderUpdated)
        {
            // echo "success";
            // $_SESSION['messageType'] = 1;
            // header('Location: ../adminShipping.php?type=11');

            if(isset($_POST['withdrawal_id']))
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($refundCredit)
                {
                    array_push($tableName,"credit");
                    array_push($tableValue,$refundCredit);
                    $stringType .=  "s";
                }   
                
                array_push($tableValue,$withdrawalUid);
                $stringType .=  "s";
                $refundUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                
                if($refundUpdated)
                {
                    header('Location: ../adminWithdrawal.php');
                }
                else
                {
                    echo "<script>alert('Fail to refund');window.location='../adminWithdrawal.php'</script>";
                }
            }
            else
            {
                echo "<script>alert('ERROR during refund');window.location='../adminWithdrawal.php'</script>";
            }

        }
        else
        {
            echo "<script>alert('unable to update user credit');window.location='../adminWithdrawal.php'</script>";
        }
    }
    else
    {
        echo "<script>alert('server error');window.location='../adminWithdrawal.php'</script>";
    }
}
else 
{
    header('Location: ../index.php');
}

?>