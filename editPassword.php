<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/editPassword.php" />
    <meta property="og:title" content="Edit Password | De Xin Guo Ji 德鑫国际" />
    <title>Edit Password | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/editPassword.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
    <?php include 'headerAfterLogin.php'; ?>

    <h1 class="menu-distance h1-title white-text text-center"><?php echo _HEADER_CHANGE_PASSWORD ?></h1>
    <div class="width100 overflow blue-opa-bg padding-box radius-box smaller-box">

        <!-- <form> -->
        <form action="utilities/editPasswordFunction.php" method="POST">
            <p class="input-top-text"><?php echo _JS_CURRENT_PASSWORD ?></p>
            <input class="clean de-input" type="password" placeholder="<?php echo _JS_CURRENT_PASSWORD ?>" id="editPassword_current" name="editPassword_current" required>   

            <div class="clear"></div> 

            <div class="dual-input-div">
                <p class="input-top-text"><?php echo _JS_NEW_PASSWORD ?></p>
                <input class="clean de-input" type="password" placeholder="<?php echo _JS_NEW_PASSWORD ?>" id="editPassword_new" name="editPassword_new" required>
            </div>

            <div class="dual-input-div second-dual-input">
                <p class="input-top-text"><?php echo _JS_RETYPE_PASSWORD ?></p>
                <input class="clean de-input" type="password" placeholder="<?php echo _JS_RETYPE_PASSWORD ?>" id="editPassword_reenter" name="editPassword_reenter" required>
            </div>

            <div class="clear"></div>

            <button class="clean blue-button mid-button-width small-distance small-distance-bottom"><?php echo _JS_SUBMIT ?></button>

            <div class="clear"></div>

        </form>

	</div>
</div>
<?php include 'js.php'; ?>

</body>
</html>