<?php
if (session_id() == ""){
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/" />
    <meta property="og:title" content="De Xin Guo Ji 德鑫国际" />
    <title>De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/" />
	<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance bg1">
	<h1 class="banner1-h1 white-text"><?php echo _INDEX_BANNER_H11 ?><br><?php echo _INDEX_BANNER_H12 ?></h1>
	<p class="banner1-p white-text"><?php echo _INDEX_BANNER_P ?></p>
</div>
<div class="width100 same-padding black-bg padding-bg">
	<h1 class="h1-title white-text text-center"><?php echo _INDEX_WHY_TRADE_WITH_US ?></h1>
    <div class="center-50div left-50div">
    	<h3><?php echo _INDEX_EXCELLENT_EXE ?></h3>
        <p><?php echo _INDEX_EXCELLENT_EXE_P ?></p>
    </div>
    <div class="center-50div right-50div">
    	<h3><?php echo _INDEX_AUTO ?></h3>
        <p><?php echo _INDEX_AUTO_P ?></p>
    </div>
    <div class="clear"></div>
    <div class="center-50div left-50div">
    	<h3><?php echo _INDEX_UI ?></h3>
        <p><?php echo _INDEX_UI_P ?></p>
    </div>    
    <div class="center-50div right-50div">
    	<h3><?php echo _INDEX_NO_DEALER ?></h3>
        <p><?php echo _INDEX_NO_DEALER_P ?></p>
    </div>    
    
    
</div>
<div class="clear"></div>
<div class="width100 overflow same-padding bg2 padding-bg">
	<h1 class="h1-title white-text text-center"><?php echo _INDEX_HOW_TRADE ?></h1>
	<div class="white-circle"><p>1</p></div><p class="trade-p"><?php echo _INDEX_STEP1 ?></p>
    <div class="clear"></div>
    <div class="white-circle"><p>2</p></div><p class="trade-p"><?php echo _INDEX_STEP2 ?></p>
    <div class="clear"></div>
    <div class="white-circle"><p>3</p></div><p class="trade-p"><?php echo _INDEX_STEP3 ?></p>
    <div class="clear"></div>
    <div class="white-circle"><p>4</p></div><p class="trade-p"><?php echo _INDEX_STEP4 ?></p>
</div>


 <?php include 'js.php'; ?>
</body>
</html>