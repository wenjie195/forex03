<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$userSMSDetails = getMessage($conn);
// $userSMSDetails = $messageRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/adminReplyMessage.php" />
    <meta property="og:title" content="View Message | De Xin Guo Ji 德鑫国际" />
    <title>View Message | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminReplyMessage.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
    <?php include 'headerAdmin.php'; ?>
    <h1 class="menu-distance h1-title white-text text-center hover-a"  onclick="goBack()"><img src="img/back.png" class="back-btn" alt="Back" title="Back"> View Message</h1>

    <div class="width100 overflow blue-opa-bg padding-box radius-box chat-div-padding">
		   
            <?php
            $conn = connDB();
            $userSMSDetails = getMessage($conn,"WHERE uid = ? ", array("uid") ,array($_POST['message_uid']),"s");
            // $senderUID = $userSMSDetails[0]->getUid();
            // $messageUID = $userSMSDetails[0]->getMessageUid();
            ?>
            <div class="fix-scroll-msg">    
                <?php
                if($userSMSDetails)
                {   
                    for($cnt = 0;$cnt < count($userSMSDetails) ;$cnt++)
                    {
                        if($userSMSDetails[$cnt]->getReplyTwo() == 'YES')
                        { 
                        ?>
                            <div class="user-chat-img">
                                <a href="./uploads/<?php echo $userSMSDetails[$cnt]->getReceiveSMS();?>" data-fancybox >
                                    <img src="uploads/<?php echo $userSMSDetails[$cnt]->getReceiveSMS();?>" class="chat-img">
                                </a>
                            	
                            </div>
                            <div class="user-chat-bubble"><?php echo $userSMSDetails[$cnt]->getReplySMS();?></div>
                        <?php
                        }
                        else
                        {
                        ?>
                            <div class="admin-chat-bubble"><?php echo $userSMSDetails[$cnt]->getReceiveSMS();?></div>                
                            <div class="user-chat-bubble"><?php echo $userSMSDetails[$cnt]->getReplySMS();?></div>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>

            </div>

            <div class="clear"></div>

            <div class="fix-msg-box">
            <?php
            $conn = connDB();
            $userSMSDetailsAA = getMessage($conn,"WHERE uid = ? ORDER BY date_created DESC", array("uid") ,array($_POST['message_uid']),"s");
            $senderUID = $userSMSDetailsAA[0]->getUid();
            $messageUID = $userSMSDetailsAA[0]->getMessageUid();
            ?>
                <form action="utilities/replyMessageFunction.php" method="POST">
                    <input class="clean de-input left-msg" type="text" placeholder="<?php echo _VIEWMESSAGE_UR_MESSAGE ?>" id="message_details" name="message_details" required>
                    <input type="hidden" value="<?php echo $senderUID;?>" id="sender_uid" name="sender_uid" readonly>
                    <input type="hidden" value="<?php echo $messageUID;?>" id="message_uid" name="message_uid" readonly>
                    <button class="clean hover1 blue-button smaller-font right-submit" type="submit" name="reply_sms">
                        <?php echo _VIEWMESSAGE_SENT2 ?>
                    </button>
                </form>
            <?php
            $conn->close();
            ?>
            </div>
    
	</div>

</div>
<style>
::-webkit-scrollbar {
  width: 3px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #15212d; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #15212d; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #15212d; 
}
</style>
<?php include 'js.php'; ?>
</body>
</html>