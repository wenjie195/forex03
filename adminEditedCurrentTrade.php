<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/timezone.php';

require_once dirname(__FILE__) . '/classes/BetStatus.php';
require_once dirname(__FILE__) . '/classes/BuySell.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();



// //30sec
// $tradeDetailsA = getBetstatus($conn," WHERE timeline = '30' AND result_edited = 'DRAW' AND status != 'ORI' ORDER BY amount DESC");
// $tradeDetailsAWin = getBetstatus($conn," WHERE timeline = '30' AND result_edited = 'LOSE' AND status != 'ORI' ORDER BY amount DESC");
// $tradeDetailsALose = getBetstatus($conn," WHERE timeline = '30' AND result_edited = 'WIN' AND status != 'ORI' ORDER BY amount DESC");
// //60sec
// $tradeDetailsB = getBetstatus($conn," WHERE timeline = '60' AND result_edited = 'DRAW' AND status != 'ORI' ORDER BY amount DESC");
// $tradeDetailsBWin = getBetstatus($conn," WHERE timeline = '60' AND result_edited = 'LOSE' AND status != 'ORI' ORDER BY amount DESC");
// $tradeDetailsBLose = getBetstatus($conn," WHERE timeline = '60' AND result_edited = 'WIN' AND status != 'ORI' ORDER BY amount DESC");
// //180sec
// $tradeDetailsC = getBetstatus($conn," WHERE timeline = '180' AND result_edited = 'DRAW' AND status != 'ORI' ORDER BY amount DESC");
// $tradeDetailsCWin = getBetstatus($conn," WHERE timeline = '180' AND result_edited = 'LOSE' AND status != 'ORI' ORDER BY amount DESC");
// $tradeDetailsCLose = getBetstatus($conn," WHERE timeline = '180' AND result_edited = 'WIN' AND status != 'ORI' ORDER BY amount DESC");

//30sec


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Current Trade | De Xin Guo Ji 德鑫国际" />
    <title>Current Trade | De Xin Guo Ji 德鑫国际</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAdmin.php'; ?>
    <?php include 'sidebar.php'; ?>
    <div class="width100 same-padding2">
    <h1 class="menu-distance h1-title white-text text-center"><a href="adminCurrentTrade.php"><?php echo _SIDEBAR_CURRENT_TRADE ?></a> | <span class="blue-link"><?php echo _ADMINCURRENTTRADE_EDITEDCT ?></span> </h1>
    <div id="divSecond" class="width100 overflow blue-opa-bg padding-box radius-box">

    </div>
    </div>
</div>
<?php include 'js.php'; ?>
</body>
</html>
<script type="text/javascript">
$(document).ready(function(){
 $("#divSecond").load("divSecondSec.php");
    setInterval(function() {
        $("#divSecond").load("divSecondSec.php");
    }, 1000);
    // $("#divSecond").load("divSecondSec.php");
    //    setInterval(function() {
    //        $("#divSecond").load("divSecondSec.php");
    //    }, 1000);
});
</script>
