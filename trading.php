<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/timezone.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<!-- <?php //include 'meta.php'; ?> -->
    <meta property="og:url" content="https://dxforextrade88.com/trading.php" />
    <meta property="og:title" content="Trading | De Xin Guo Ji 德鑫国际" />
    <title>Trading | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/trading.php" />
	<!-- <?php //include 'css.php'; ?> -->
</head>
<body class="body">


<?php //echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!-- <?php //include 'adminSidebar.php'; ?> -->

<div class="next-to-sidebar">
    <h1 class="h1-title">Select Rate and Trade</h1>

        <form   action="utilities/submitTradeFunction.php" method="POST">
            <div class="input50-div">
                <p class="input-title-p">Rate</p>
                <input class="clean tele-input" type="text" id="trade_rate" name="trade_rate" value="4.0" readonly>
            </div>

            <div class="clear"></div>

            <div class="input50-div second-input50">
                <p class="input-title-p">Currency</p>
                <input class="clean tele-input"  type="text" placeholder="Currency" id="trade_currency" name="trade_currency" required>
            </div>

            <div class="clear"></div>

            <div class="input50-div">
                <p class="input-title-p">Trade Amount</p>
                <input class="clean tele-input"  type="text" placeholder="Trade Amount" id="trade_amount" name="trade_amount" required>
            </div>

            <div class="clear"></div>

            <div class="input50-div">
                <p class="input-title-p">Trade Type</p>
                <select class="clean tele-input" type="text" id="trade_type" name="trade_type" required>
                    <option value="" name=" ">PLEASE SELECT TRADE TYPE</option>
                    <option value="Buy" name="Buy">Buy</option>
                    <option value="Sell" name="Sell">Sell</option>
                </select>
            </div>

            <div class="clear"></div>

            <div class="input50-div">
                <p class="input-title-p">Timeline</p>
                <select class="clean tele-input" type="text" id="trade_timeline" name="trade_timeline" required>
                    <option value="" name=" ">PLEASE SELECT A TIMELINE</option>
                    <option value="30" name="30">30 SEC</option>
                    <option value="60" name="60">60 SEC</option>
                    <option value="180" name="180">180 SEC</option>
                </select>
            </div>

            <div class="clear"></div>

            <!-- <div class="input50-div">
                <p class="input-title-p">Time</p>
                <input class="clean tele-input"  type="date" placeholder="Time" id="deposit_date" name="deposit_date">
                <input class="clean tele-input"  type="time" placeholder="Time" id="deposit_time" name="deposit_time">
            </div>

            <div class="clear"></div>

            <div class="input50-div">
                <p class="input-title-p">Reference</p>
                <input class="clean tele-input"  type="text" placeholder="Reference" id="deposit_reference" name="deposit_reference">
            </div>

            <div class="clear"></div> -->
            <input type="hidden" name="date" value="<?php echo date('Y-m-d H:i:s'); ?>">
            <input class="clean tele-input"  type="text" id="user_uid" name="user_uid" value="<?php echo $userDetails->getUid();?>" readonly>
            <input class="clean tele-input"  type="text" id="user_credit" name="user_credit" value="<?php echo $userDetails->getCredit();?>" readonly>

            <div class="clear"></div>

            <button class="clean red-btn margin-top30 fix300-btn" name="refereeButton">Submit</button>
        </form>


</div>

<style>
.account-li{
	color:#bf1b37;
	background-color:white;}
.account-li .hover1a{
	display:none;}
.account-li .hover1b{
	display:block;}
</style>

<!-- <?php //include 'js.php'; ?> -->

</body>
</html>
