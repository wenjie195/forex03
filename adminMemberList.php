<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE user_type =1 ORDER BY date_created DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/addReferee.php" />
    <meta property="og:title" content="Admin Member List | De Xin Guo Ji 德鑫国际" />
    <title>Admin Member List | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/addReferee.php" />

    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAdmin.php'; ?>
    <?php include 'sidebar.php'; ?>
    <div class="width100 same-padding2">
    <!-- <h1 class="menu-distance h1-title white-text text-center">Member List</h1> -->
    <h1 class="menu-distance h1-title white-text text-center"><?php echo _SIDEBAR_CUSTOMER_LIST ?></h1>
    <div class="width100 overflow blue-opa-bg padding-box radius-box">

    <div class="clear"></div>

        <div class="width100 shipping-div2">
            <div class="overflow-scroll-div">
                <table class="table-width data-table message-table">
                    <thead>
                        <tr>
                            <th><?php echo _VIEWMESSAGE_NO ?></th>
                            <th><?php echo _JS_USERNAME ?></th>
                            <th><?php echo _JS_PHONE ?></th>
                            <th><?php echo _JS_EMAIL ?></th>
                            <!-- <th><?php //echo _VIEWMESSAGE_DATE ?></th> -->
                            <!-- <th><?php //echo _ADMINMEMBER_CREDIT ?></th>
                            <th><?php //echo _ADMINMEMBER_ADDCREDIT ?></th>
                            <th>Deposit History</th> -->
                            <!-- <th>WIN</th>
                            <th>LOSE</th>
                            <th>NETT GAIN / LOSE</th> -->
                            <th><?php echo _ADMINMEMBER_LASTUP ?></th>
                            <!-- <th>Member Status</th> -->
                            <!-- <th><?php //echo _ADMINMEMBER_DETAILS ?></th> -->
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if($userRows)
                        {   
                            for($cnt = 0;$cnt < count($userRows) ;$cnt++)
                            {?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $userRows[$cnt]->getUsername();?></td>
                                <td><?php echo $userRows[$cnt]->getPhoneNo();?></td>
                                <td><?php echo $userRows[$cnt]->getEmail();?></td>
                                <!-- <td><?php //echo $userRows[$cnt]->getDateCreated();?></td> -->

                                <!-- <td><?php //echo $userRows[$cnt]->getCredit();?></td>

                                <td>
                                    <form action="adminUserAddCredit.php" method="POST">
                                        <button class="clean hover1 blue-button smaller-font" type="submit" name="user_uid" value="<?php echo $userRows[$cnt]->getUid();?>">
                                            <?php //echo _ADMINMEMBER_TOPUPCREDIT ?>
                                        </button>
                                    </form>
                                </td>

                                <td>
                                    <form action="adminUserDepositHistory.php" method="POST">
                                        <button class="clean hover1 blue-button smaller-font" type="submit" name="user_username" value="<?php echo $userRows[$cnt]->getUsername();?>">
                                            Click To View History
                                        </button>
                                    </form>
                                </td> -->

                                <!-- <td></td>
                                <td></td>
                                <td></td> -->

                                <!-- <td><?php //echo $userRows[$cnt]->getDateUpdated();?></td> -->
                                <td><?php echo date('d/m/Y h:i', strtotime($userRows[$cnt]->getDateUpdated()));?></td>

                                <!-- <td><?php //echo $userRows[$cnt]->getLoginType();?></td> -->

                                <td>
                                    <form action="adminUserDetails.php" method="POST">
                                        <button class="clean hover1 blue-button smaller-font" type="submit" name="useruid_details" value="<?php echo $userRows[$cnt]->getUid();?>">
                                        <input type="hidden" id="user_username" name="user_username" value="<?php echo $userRows[$cnt]->getUsername();?>" readonly>
                                            <?php echo _ADMINMEMBER_OPERATION ?>
                                        </button>
                                    </form>
                                </td>
                                <td>
                                    <form action="adminUserTradeDetails.php" method="POST">
                                        <button class="clean hover1 blue-button smaller-font" type="submit" name="useruid_tradedetails" value="<?php echo $userRows[$cnt]->getUid();?>">
                                        <input type="hidden" id="user_username" name="user_username" value="<?php echo $userRows[$cnt]->getUsername();?>" readonly>
                                            <?php echo _ADMINMEMBER_CLICKDETAILS ?>
                                        </button>
                                    </form>
                                </td>

                            <?php
                            }?>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
</div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "此用户的户口已被激活！";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "此用户的户口已被停用！";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "无法更新此用户的户口状体！";
        }  
        else if($_GET['type'] == 3)
        {
            $messageType = "出了点小状况，请稍后再试！";
        }       
        echo '
        <script>
            putNoticeJavascript("通告 !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;

    }
}
?>

</body>
</html>