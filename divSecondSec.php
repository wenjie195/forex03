<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/timezone.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/BetStatus.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $curl = curl_init();
//
// curl_setopt_array($curl, array(
// 	CURLOPT_URL => "https://financialmodelingprep.com/api/v3/forex",
// 	CURLOPT_RETURNTRANSFER => true,
// 	CURLOPT_FOLLOWLOCATION => true,
// 	CURLOPT_ENCODING => "",
// 	CURLOPT_MAXREDIRS => 10,
// 	CURLOPT_TIMEOUT => 30,
// 	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
// 	CURLOPT_CUSTOMREQUEST => "GET",
// 	CURLOPT_HTTPHEADER => array(
// 		"x-rapidapi-host: currency-exchange.p.rapidapi.com",
// 		"x-rapidapi-key: c5875e8297msh333e08626d58b3fp1015afjsn73bce78f515f"
// 	),
// ));
//
// $response = curl_exec($curl);
// $err = curl_error($curl);
//
// curl_close($curl);
//
// if ($err) {
// 	echo "cURL Error #:" . $err;
// } else {
// 	$exchangeRates = json_decode($response, true);
// }
?>
<div  class="three-div-width">
<div class="fake-header-div">
    30 <?php echo _USERDASHBOARD_SEC ?>
  </div>
<table class="width100 data-table trade-table">
  <thead>
      <tr style="background-color: transparent">
          <th><?php echo _ADMINCURRENTTRADE_UNREALIZED_GAIN ?></th>
            <th><?php echo _ADMINCURRENTTRADE_UNREALIZED_LOSS ?></th>
      </tr>
    </thead>
  <tbody>
    <?php
    $betStatusDetails = getBetstatus($conn,"WHERE timeline = '30'");
    if ($betStatusDetails) {
			for ($i=0; $i <count($betStatusDetails) ; $i++) {

        $betCurrencyVal = $betStatusDetails[$i]->getStartRate();
				$tradeUID = $betStatusDetails[$i]->getTradeUid();
				$userUID = $betStatusDetails[$i]->getUid();
        $betCurrency = $betStatusDetails[$i]->getCurrency();
        $betType = $betStatusDetails[$i]->getBetType();
				$betEditBy = $betStatusDetails[$i]->getEditBy();
        $betEditResult = $betStatusDetails[$i]->getResultEdited();
				$username = $betStatusDetails[$i]->getUsername();
				$betAmount = $betStatusDetails[$i]->getAmount();
				$time = date('Y-m-d H:i:s',strtotime($betStatusDetails[$i]->getDateCreated()));
				$betTimeline = $betStatusDetails[$i]->getTimeline();
				$finishTime = date('Y-m-d H:i:s',strtotime($time."+".$betTimeline."seconds"));
				$finishTimeString = strtotime($finishTime);
				$currentTime = date('Y-m-d H:i:s');
				$currentTimeString = strtotime($currentTime);
				$timeLeft = $finishTimeString - $currentTimeString;
        if ($betEditBy && $currentTime < $finishTime) {

					if ($betEditResult == 'LOSE') {
						?><tr style="background-color: transparent">
						 <td><button class="clean green-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betStatusDetails[$i]->getAmount() ?></button> </td>
						</tr> <?php
					}elseif ($betEditResult == 'WIN') {
						?><tr style="background-color: transparent">
              <td></td>
						 <td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betStatusDetails[$i]->getAmount() ?></button> </td>
						</tr> <?php
					}
    }}}
     ?>
    <tr style="background-color: transparent">

      <!-- <td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php //echo $betStatusDetails[$i]->getTradeUid()?>">Lose</button> </td> -->
    </tr>
  </tbody>
</table>
</div>
<div class="three-div-width">
<div class="fake-header-div">
    60 <?php echo _USERDASHBOARD_SEC ?>
  </div>
<table class="width100 data-table trade-table">
  <thead>
      <tr style="background-color: transparent">
          <th><?php echo _ADMINCURRENTTRADE_UNREALIZED_GAIN ?></th>
            <th><?php echo _ADMINCURRENTTRADE_UNREALIZED_LOSS ?></th>
      </tr>
    </thead>
  <tbody>
    <?php
    $betStatusDetails60 = getBetstatus($conn,"WHERE timeline = '60'");
    if ($betStatusDetails60) {
      for ($j=0; $j <count($betStatusDetails60) ; $j++) {



        $betCurrencyVal = $betStatusDetails60[$j]->getStartRate();
        $betCurrency = $betStatusDetails60[$j]->getCurrency();
        $betType = $betStatusDetails60[$j]->getBetType();
				$betEditBy = $betStatusDetails60[$j]->getEditBy();
        $betEditResult = $betStatusDetails60[$j]->getResultEdited();
				$time = date('Y-m-d H:i:s',strtotime($betStatusDetails60[$j]->getDateCreated()));
				$betTimeline = $betStatusDetails60[$j]->getTimeline();
				$finishTime = date('Y-m-d H:i:s',strtotime($time."+".$betTimeline."seconds"));
				$currentTime = date('Y-m-d H:i:s');
				$tradeUID2 = $betStatusDetails60[$j]->getTradeUid();
				$finishTimeString = strtotime($finishTime);
				$currentTimeString = strtotime($currentTime);
				$timeLeft = $finishTimeString - $currentTimeString;
        if ($betEditBy && $currentTime < $finishTime) {
					if ($betEditResult == 'LOSE') {
						?><tr style="background-color: transparent">
						 <td><button class="clean green-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails60[$j]->getTradeUid()?>"><?php echo $betStatusDetails60[$j]->getUsername()."<br>".$betStatusDetails60[$j]->getAmount() ?></button> </td>
						</tr> <?php
					}elseif ($betEditResult == 'WIN') {
						?><tr style="background-color: transparent">
              <td></td>
						 <td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails60[$j]->getTradeUid()?>"><?php echo $betStatusDetails60[$j]->getUsername()."<br>".$betStatusDetails60[$j]->getAmount() ?></button> </td>
						</tr> <?php
					}
    }}}
     ?>
    <tr style="background-color: transparent">

      <!-- <td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php //echo $betStatusDetails60[$j]->getTradeUid()?>">Lose</button> </td> -->
    </tr>
  </tbody>
</table>
	</div>

  <div  class="three-div-width">
  <div class="fake-header-div">
      180 <?php echo _USERDASHBOARD_SEC ?>
    </div>
  <table class="width100 data-table trade-table">
    <thead>
        <tr style="background-color: transparent">
            <th><?php echo _ADMINCURRENTTRADE_UNREALIZED_GAIN ?></th>
              <th><?php echo _ADMINCURRENTTRADE_UNREALIZED_LOSS ?></th>
        </tr>
      </thead>
    <tbody>
      <?php
      $betStatusDetails = getBetstatus($conn,"WHERE timeline = '180'");
      if ($betStatusDetails) {
  			for ($i=0; $i <count($betStatusDetails) ; $i++) {

          $betCurrencyVal = $betStatusDetails[$i]->getStartRate();
  				$tradeUID = $betStatusDetails[$i]->getTradeUid();
  				$userUID = $betStatusDetails[$i]->getUid();
          $betCurrency = $betStatusDetails[$i]->getCurrency();
          $betType = $betStatusDetails[$i]->getBetType();
  				$betEditBy = $betStatusDetails[$i]->getEditBy();
          $betEditResult = $betStatusDetails[$i]->getResultEdited();
  				$username = $betStatusDetails[$i]->getUsername();
  				$betAmount = $betStatusDetails[$i]->getAmount();
  				$time = date('Y-m-d H:i:s',strtotime($betStatusDetails[$i]->getDateCreated()));
  				$betTimeline = $betStatusDetails[$i]->getTimeline();
  				$finishTime = date('Y-m-d H:i:s',strtotime($time."+".$betTimeline."seconds"));
  				$finishTimeString = strtotime($finishTime);
  				$currentTime = date('Y-m-d H:i:s');
  				$currentTimeString = strtotime($currentTime);
  				$timeLeft = $finishTimeString - $currentTimeString;
          if ($betEditBy && $currentTime < $finishTime) {

  					if ($betEditResult == 'LOSE') {
  						?><tr style="background-color: transparent">
  						 <td><button class="clean green-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betStatusDetails[$i]->getAmount() ?></button> </td>
  						</tr> <?php
  					}elseif ($betEditResult == 'WIN') {
  						?><tr style="background-color: transparent">
                <td></td>
  						 <td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betStatusDetails[$i]->getAmount() ?></button> </td>
  						</tr> <?php
  					}
      }}}
       ?>
      <tr style="background-color: transparent">

        <!-- <td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php //echo $betStatusDetails[$i]->getTradeUid()?>">Lose</button> </td> -->
      </tr>
    </tbody>
  </table>
  </div>
