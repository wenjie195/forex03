<?php
$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => "https://financialmodelingprep.com/api/v3/forex",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
		"x-rapidapi-host: currency-exchange.p.rapidapi.com",
		"x-rapidapi-key: c5875e8297msh333e08626d58b3fp1015afjsn73bce78f515f"
	),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	$exchangeRates = json_decode($response, true);
}

 ?>
