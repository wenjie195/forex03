<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Deposit.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/topupHistory.php" />
    <meta property="og:title" content="Top Up History | De Xin Guo Ji 德鑫国际" />
    <title>Top Up History | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/topupHistory.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAfterLogin.php'; ?>
    <div class="overflow small-web-width menu-distance">
    <h1 class="white-text history-title"><?php echo _PROFILE_TOPUP_HISTORY ?></h1>
		<table class="table-width data-table">
        	<thead>
            	<tr>
                	<th><?php echo _AUD_AMOUNT ?></th>
                    <th><?php echo _TOPUP_HISTORY_DATE ?></th>
                </tr>
            </thead> 
            <!-- <tbody>
            	<tr>
                	<td>1000</td>
                    <td>1/1/2020 10:00</td>
                </tr>
            	<tr>
                	<td>1000</td>
                    <td>1/1/2020 10:00</td>
                </tr>                
            </tbody> -->

            <tbody>
                <?php
                $conn = connDB();
                $userDetails = $userRows[0];
                // $userUsername = $userDetails[0]->getUsername();
                $userUsername = $userDetails->getUsername();

                $depositArray = getDeposit($conn,"WHERE username = ? ORDER BY date_created desc", array("username") ,array($userUsername),"s");
                if($depositArray != null)
                {   
                    for($cnt = 0;$cnt < count($depositArray) ;$cnt++)
                    {
                    ?>
                    <tr>
                        <td><?php echo $depositArray[$cnt]->getAmount();?></td>
                        <!-- <td><?php //echo $depositArray[$cnt]->getDateCreated();?></td> -->

                        <td>
                            <?php $dateCreated = date("Y-m-d H:i",strtotime($depositArray[$cnt]->getDateCreated())); echo $dateCreated;?>
                        </td>

                    <?php
                    }
                    ?>
                    </tr>
                <?php
                }
                ?>   
            </tbody>
            
         </table>   
    </div> 
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>

</html>
