<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BetStatus.php';
require_once dirname(__FILE__) . '/classes/BuySell.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
$time = $dt->format('Y-m-d H:i:s');

//additional hrs or mins
// $cenvertedTimeMin = date('Y-m-d H:i:s',strtotime('+30 second',strtotime($time)));
// $cenvertedTimeMin = date('Y-m-d H:i:s',strtotime('-11 minutes',strtotime($time)));
$cenvertedTimeMin = date('Y-m-d H:i:s',strtotime('-180 seconds',strtotime($time)));
// $aassdd = getTimeTeleUpdate($conn," WHERE update_status = 'Good' AND date_created >= '".$cenvertedTimeMin."' ");

// $tradeDetails = getBetstatus($conn," WHERE date_created >= '".$cenvertedTimeMin."' ");

$tradeDetails = getBetstatus($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<!-- <?php //include 'meta.php'; ?> -->
    <meta property="og:url" content="https://dxforextrade88.com/adminCheckTrade.php" />
    <meta property="og:title" content="Trade| De Xin Guo Ji 德鑫国际" />
    <title>Trade| De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminCheckTrade.php" />
	<!-- <?php //include 'css.php'; ?> -->
</head>
<body class="body">

<?php //echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!-- <?php //include 'adminSidebar.php'; ?> -->

<div class="next-to-sidebar">
    <h1 class="h1-title">Trade Status</h1>
    <h5 class="h1-title"><?php echo $time;?></h5>
    <h5 class="h1-title"><?php echo $cenvertedTimeMin;?></h5>

    <h5 class="h1-title">Result 'WIN' is admin lose</h5>
    <h5 class="h1-title">Result 'LOSE' is admin win</h5>

    <h5 class="h1-title">Edited Result 'Lose' is admin win</h5>


    <div class="clear"></div>

<div class="width100 overflow">
    <?php $conn = connDB();?>
    <table class="shipping-table">    
        <thead>
            <tr>
                <th>NO.</th>
                <th>TIMELINE</th>
                <th>CURRENCY</th>
                <th>TRADE TYPE</th>
                <th>AMOUNT</th>
                <th>START RATE</th>
                <th>END RATE</th>
                <th>RESULT</th>
                <th>EDITED RESULT</th>
                <th>VERIFY</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if($tradeDetails != null)
            {
            for($cnt = 0;$cnt < count($tradeDetails) ;$cnt++)
            {?>
            <tr>
                <td><?php echo ($cnt+1)?></td>
                <td><?php echo $tradeDetails[$cnt]->getTimeline();?> sec</td>
                <td><?php echo $tradeDetails[$cnt]->getCurrency();?></td>
                <td><?php echo $tradeDetails[$cnt]->getBetType();?></td>
                <td><?php echo $tradeDetails[$cnt]->getAmount();?></td>
                <td><?php echo $tradeDetails[$cnt]->getStartRate();?></td>
                <td><?php echo $tradeDetails[$cnt]->getEndRate();?></td>
                <td><?php echo $tradeDetails[$cnt]->getResult();?></td>
                <td><?php echo $tradeDetails[$cnt]->getResultEdited();?></td>

                <td>                                
                    <?php
                    $editRes = $tradeDetails[$cnt]->getResultEdited();
                    if($editRes == '')
                    {
                    ?>
                        <form action="utilities/adminEditResultFunction.php" method="POST">
                            <input type="hidden" id="trading_uid" name="trading_uid" value="<?php echo $tradeDetails[$cnt]->getTradeUid()?>">
                            <input type="hidden" id="trading_userid" name="trading_userid" value="<?php echo $tradeDetails[$cnt]->getUid()?>">
                            <input type="hidden" id="trading_id" name="trading_id" value="<?php echo $tradeDetails[$cnt]->getId()?>">
                            <input type="hidden" id="trading_amount" name="trading_amount" value="<?php echo $tradeDetails[$cnt]->getAmount()?>">
                            <input type="hidden" id="trading_result" name="trading_result" value="<?php echo $tradeDetails[$cnt]->getResult()?>">

                            <button class="clean edit-anc-btn hover1" type="submit" name="trade_uid" value="<?php echo $tradeDetails[$cnt]->getTradeUid();?>">
                                <img src="img/verify-payment.png" class="edit-announcement-img hover1a" alt="Edit Result" title="Edit Result">
                                <img src="img/verify-payment2.png" class="edit-announcement-img hover1b" alt="Edit Result" title="Edit Result">
                            </button>
                        </form>
                    <?php
                    }
                    else
                    {?>
                        <!-- EDIT AGAIN -->
                        <form action="utilities/adminRevertEditResultFunction.php" method="POST">
                            <input type="hidden" id="trading_uid" name="trading_uid" value="<?php echo $tradeDetails[$cnt]->getTradeUid()?>">
                            <input type="hidden" id="trading_userid" name="trading_userid" value="<?php echo $tradeDetails[$cnt]->getUid()?>">
                            <input type="hidden" id="trading_id" name="trading_id" value="<?php echo $tradeDetails[$cnt]->getId()?>">
                            <input type="hidden" id="trading_amount" name="trading_amount" value="<?php echo $tradeDetails[$cnt]->getAmount()?>">
                            <input type="hidden" id="trading_result" name="trading_result" value="<?php echo $tradeDetails[$cnt]->getResult()?>">
                            <input type="hidden" id="trading_editresult" name="trading_editresult" value="<?php echo $tradeDetails[$cnt]->getResultEdited()?>">

                            <button class="clean edit-anc-btn hover1" type="submit" name="trade_uid" value="<?php echo $tradeDetails[$cnt]->getTradeUid();?>">
                                <img src="img/verify-payment.png" class="edit-announcement-img hover1a" alt="Edit Result" title="Edit Result">
                                <img src="img/verify-payment2.png" class="edit-announcement-img hover1b" alt="Edit Result" title="Edit Result">
                            </button>
                        </form>

                    <?php
                    }
                    ?>
                </td>

                <!-- <td>
                    <form action="utilities/adminEditResultFunction.php" method="POST">
                        <input type="hidden" id="trading_uid" name="trading_uid" value="<?php echo $tradeDetails[$cnt]->getTradeUid()?>">
                        <input type="hidden" id="trading_userid" name="trading_userid" value="<?php echo $tradeDetails[$cnt]->getUid()?>">
                        <input type="hidden" id="trading_id" name="trading_id" value="<?php echo $tradeDetails[$cnt]->getId()?>">
                        <input type="hidden" id="trading_amount" name="trading_amount" value="<?php echo $tradeDetails[$cnt]->getAmount()?>">
                        <input type="hidden" id="trading_result" name="trading_result" value="<?php echo $tradeDetails[$cnt]->getResult()?>">

                        <button class="clean edit-anc-btn hover1" type="submit" name="trade_uid" value="<?php //echo $tradeDetails[$cnt]->getTradeUid();?>">
                            <img src="img/verify-payment.png" class="edit-announcement-img hover1a" alt="Edit Result" title="Edit Result">
                            <img src="img/verify-payment2.png" class="edit-announcement-img hover1b" alt="Edit Result" title="Edit Result">
                        </button>
                    </form>
                </td> -->
            </tr>
            <?php
            }
            }
            ?>
        </tbody>
    </table>
    <?php $conn->close();?>
</div>

    <div class="clear"></div>
</div>

<style>
.account-li{
	color:#bf1b37;
	background-color:white;}
.account-li .hover1a{
	display:none;}
.account-li .hover1b{
	display:block;}
</style>

<!-- <?php //include 'js.php'; ?> -->

</body>
</html>